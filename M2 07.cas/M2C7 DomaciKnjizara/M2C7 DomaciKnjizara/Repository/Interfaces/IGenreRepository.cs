﻿using M2C7_DomaciKnjizara.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2C7_DomaciKnjizara.Repository.Interfaces
{
    interface IGenreRepository
    {
        IEnumerable<Genre> GetAll();
        Genre GetById(int id);
        bool Create(Genre Genre);
        void Update(Genre Genre);
        void Delete(int id);
    }
}
