﻿using M2C7_DomaciKnjizara.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2C7_DomaciKnjizara.ViewModels
{
    public class BookGenreViewModel
    {
        public Book Book { get; set; } = new Book();
        public Genre Genre { get; set; } = new Genre();
        public List<Genre> GenreList { get; set; } = new List<Genre>();
        public int SelectedGenreId { get; set; }
    }
}