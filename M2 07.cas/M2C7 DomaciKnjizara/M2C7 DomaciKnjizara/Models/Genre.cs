﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2C7_DomaciKnjizara.Models
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}