﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2C7_DomaciKnjizara.Models
{
    public class Book
    {
        //public static int SId = 0;
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public Genre Genre { get; set; }
        public bool Deleted { get; set; }
       
        public Book()
        {
        }

        public Book(int id, string name, int price, Genre genre, bool deleted)
        {
            Id = id;
            Name = name;
            Price = price;
            Genre = genre;
            Deleted = deleted;
        }

        public Book(string name, int price, Genre genre)
        {
            //Id = SId++;
            Name = name;
            Price = price;
            Genre = genre;
            Deleted = false;
        }

        

    }
}