﻿using Knjizara_Domaci.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Knjizara_Domaci.Controllers
{
    public class SearchController : Controller
    {
        // GET: Search
        public ActionResult Index()
        {
            ViewBag.Listaknjiga = Bookstore.Listaknjiga;
            // Bookstore.Listaknjiga.
            return View();
        }

        public ActionResult Search(string search, string criteria)
        {
            List<Book> FoundBooks = null;
            List<Chapter> FoundChapters = null;
            if (criteria == ("book"))
            {
                foreach (var item in Bookstore.Listaknjiga.Values)
                {
                    FoundBooks = new List<Book>();
                    if (item.Name.Equals(search)) FoundBooks.Add(item);
                }
            }
            else
            {
                foreach (var item in Bookstore.Listaknjiga.Values)
                {
                    foreach (var poglavlje in item.ListaPoglavlja)
                    {
                        FoundChapters = new List<Chapter>();
                        if (poglavlje.ChapterName.Equals(search)) FoundChapters.Add(poglavlje);
                    }

                }
            }

            ViewBag.search = search;
            ViewBag.criteria = criteria;
            ViewBag.FoundBooks = FoundBooks;
            ViewBag.FoundChapters = FoundChapters;
            return View();
        }



        public ActionResult Sort(string BookName, string BookPrice)
        {
            if (BookName == null && BookPrice == null)
            {
                return RedirectToAction("List");
            }
            else if (BookName != null && BookPrice == null)
            {
                IOrderedEnumerable<KeyValuePair<int, Book>> sortedDict = null;
                if (BookName == "asc")
                {
                    sortedDict = from entry in Bookstore.Listaknjiga orderby entry.Value.Name ascending select entry;
                }
                else
                {
                    sortedDict = from entry in Bookstore.Listaknjiga orderby entry.Value.Name descending select entry;
                }
                ViewBag.Listaknjiga = sortedDict.ToDictionary(i => i.Key, i => i.Value);
                return View("List");
            }
            else if (BookName == null && BookPrice != null)
            {
                IOrderedEnumerable<KeyValuePair<int, Book>> sortedDict = null;
                if (BookPrice == "asc")
                {
                    sortedDict = from entry in Bookstore.Listaknjiga orderby entry.Value.Price ascending select entry;
                }
                else
                {
                    sortedDict = from entry in Bookstore.Listaknjiga orderby entry.Value.Price descending select entry;
                }
                ViewBag.Listaknjiga = sortedDict.ToDictionary(i => i.Key, i => i.Value);
                return View("List");
            }
            else if (BookName != null && BookPrice != null)
            {
                IOrderedEnumerable<KeyValuePair<int, Book>> sorted = null;
                if (BookName == "asc" && BookPrice == "asc")
                {
                    sorted = Bookstore.Listaknjiga.OrderBy(x => x.Value.Name).ThenBy(x => x.Value.Price);
                }
                else if (BookName == "asc" && BookPrice == "des")
                {
                    sorted = Bookstore.Listaknjiga.OrderBy(x => x.Value.Name).ThenByDescending(x => x.Value.Price);
                }
                else if (BookName == "des" && BookPrice == "asc")
                {
                    sorted = Bookstore.Listaknjiga.OrderByDescending(x => x.Value.Name).ThenBy(x => x.Value.Price);
                }
                else if (BookName == "des" && BookPrice == "des")
                {
                    sorted = Bookstore.Listaknjiga.OrderByDescending(x => x.Value.Name).ThenByDescending(x => x.Value.Price);
                }

                ViewBag.Listaknjiga = sorted.ToDictionary(i => i.Key, i => i.Value);
                return View("List");
            }


            return View();
        }



    }
}