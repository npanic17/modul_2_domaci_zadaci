﻿using Knjizara_Domaci.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Knjizara_Domaci.Controllers
{
    public class ChapterController : Controller
    {
        // GET: Chapter
        public ActionResult Index(int id)
        {
            ViewBag.id = id;
            return View();
        }

        //[HttpPost]
        public ActionResult AddChapter(string ChapterName, int NumberOfPages, int id)
        {
            Chapter poglavlje = new Chapter(ChapterName, NumberOfPages);
            Bookstore.Listaknjiga[id].ListaPoglavlja.Add(poglavlje);
            ViewBag.Listaknjiga = Bookstore.Listaknjiga;

            return View("List"); 
        }

        public ActionResult ListChapter(int id)
        {
            ViewBag.id = id;
            ViewBag.Listaknjiga = Bookstore.Listaknjiga;
            return View();
        }
    }
}