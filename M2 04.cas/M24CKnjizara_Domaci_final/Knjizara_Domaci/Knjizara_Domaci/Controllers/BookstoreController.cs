﻿using Knjizara_Domaci.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Knjizara_Domaci.Controllers
{
    public class BookstoreController : Controller
    {
        // GET: Bookstore
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult List()
        {
            ViewBag.Listaknjiga = Bookstore.Listaknjiga;
            return View();
        }

        public ActionResult Deleted()
        {
            ViewBag.Listaknjiga = Bookstore.Listaknjiga;
            return View();
        }

        [HttpPost]
        public ActionResult Add(string imeknjige, int cenaknjige, string zanrknjige)
        {
            if (zanrknjige == "1") zanrknjige = "Science";
            else if (zanrknjige == "2") zanrknjige = "Comedy";
            else zanrknjige = "Horror";

            if (Bookstore.Listaknjiga != null)
            {
                foreach(var item in Bookstore.Listaknjiga)
                {
                    if (imeknjige == item.Value.Name) return View("Existing");
                }
            }

            Book knjiga = new Book(imeknjige, cenaknjige, zanrknjige);
            // Bookstore knjizara = new Bookstore();
            Bookstore.Listaknjiga.Add(knjiga.Id, knjiga);
            ViewBag.Listaknjiga = Bookstore.Listaknjiga;

            return View("List");
        }


        public ActionResult Delete(int id)
        {
            // int index = Int32.Parse(id);
            Bookstore.Listaknjiga[id].Deleted = true;
            ViewBag.Listaknjiga = Bookstore.Listaknjiga;
            return RedirectToAction("List");
        }


    }
}