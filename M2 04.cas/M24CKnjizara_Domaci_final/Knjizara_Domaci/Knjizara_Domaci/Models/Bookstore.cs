﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara_Domaci.Models
{
    public class Bookstore
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public static Dictionary<int,Book> Listaknjiga { get; set; }= new Dictionary<int,Book>();
       
    }
}