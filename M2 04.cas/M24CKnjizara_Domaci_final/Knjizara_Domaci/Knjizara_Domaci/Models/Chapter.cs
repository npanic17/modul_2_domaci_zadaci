﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara_Domaci.Models
{
    public class Chapter
    {
        

        //public int Id { get; set; }
        public string ChapterName { get; set; }
        public int NumberOfPages { get; set; }

        public Chapter(string chapterName, int numberOfPages)
        {
            ChapterName = chapterName;
            NumberOfPages = numberOfPages;
        }
    }
}