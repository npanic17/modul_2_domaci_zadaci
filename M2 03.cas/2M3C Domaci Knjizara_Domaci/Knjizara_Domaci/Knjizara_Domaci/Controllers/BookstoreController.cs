﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Knjizara_Domaci.Models;

namespace Knjizara_Domaci.Controllers
{
    public class BookstoreController : Controller
    {
        // GET: Bookstore
        public ActionResult Index()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"<hr/>");
            sb.Append($"<h1>Bookstore- Home Page</h1>");
            sb.Append($"<hr/>");
            sb.Append("<form method='get' action='Add'>");
            sb.Append("<table border='1'>");
            sb.Append("<tr><th colspan = '2'> Add new book </th></ tr>");
            sb.Append("<tr><td align='left'>Name of book:</td> <td valign='middle'><input type='text' name='imeknjige'></td></tr>");
            sb.Append("<tr><td align='left'>Price of book:</td> <td valign='middle'><input type='number' name='cenaknjige'></td></tr>");
            sb.Append("<tr><td align='left'>Genre of book:</td> <td><select name='zanrknjige'><option value='1'>Science</option><option value='2'>Comedy</option> <option value='3'>Horror</option></td></tr>");
            sb.Append("<tr><td colspan= '2' align='right'> <input type='submit' name='add' value='Add'> </td></ tr>");
            sb.Append("</table>");
            sb.Append("</form>");
            sb.Append("</form>");
            sb.Append("<a href='List'> Show all books</a>");
            sb.Append("<a href='/Bookstore/Deleted'> Show Deleted books</a>");
            return Content(sb.ToString());
        }

        public RedirectResult Add(string imeknjige, int cenaknjige, string zanrknjige)
        {
            if (zanrknjige == "1") zanrknjige = "Science";
            else if (zanrknjige == "2") zanrknjige = "Comedy";
            else zanrknjige = "Horror";
            Book knjiga = new Book(imeknjige, cenaknjige, zanrknjige);
            Bookstore knjizara = new Bookstore();
            Bookstore.Listaknjiga.Add(knjiga);

            return Redirect("List");
        }


        public ActionResult List()    // jos jedan od nacina preuzimanja vrednosti promenjivih iz HTTP zahteva (frekventniji u praksi)
        {                                       // moze primiti vise parametara pr: (string num1, string num2 itd...)

            StringBuilder sb = new StringBuilder();
            sb.Append($"<hr/> ");
            sb.Append($"<h1>Showing all books</h1>");
            sb.Append($"<hr/>");
            sb.Append($"<table border='1'>");
            sb.Append($"<tr><th>Id</th><th>Name</th><th>Price</th><th>Genre</th></tr>");


            foreach (Book item in Bookstore.Listaknjiga)
            {
                if (item.Deleted == false)
                {
                    int id = item.Id;
                    sb.Append($"<tr><td>{item.Id}</td><td>{item.Name}</td><td>{item.Price}</td><td>{item.Genre}</td>");
                    sb.Append($"<td><a href='/Bookstore/Delete/{id}'>Delete</a></td></tr>");
                }

            }

            sb.Append("</table>");
            sb.Append("<a href='Index'> Link for homepage</a>");
            return Content(sb.ToString());

        }

        public RedirectResult Delete(int id)
        {
            // int index = Int32.Parse(broj);
            foreach (var item in Bookstore.Listaknjiga)
            {
                if (id == item.Id) Bookstore.Listaknjiga[id].Deleted = true;
            }


            return Redirect("/Bookstore/List");
        }

        public ActionResult Deleted()
        {

            StringBuilder sb = new StringBuilder();
            sb.Append($"<hr/> ");
            sb.Append($"<h1>Showing all deleted books</h1>");
            sb.Append($"<hr/>");
            sb.Append($"<table border='1'>");
            sb.Append($"<tr><th>Id</th><th>Name</th><th>Price</th><th>Genre</th></tr>");


            foreach (Book item in Bookstore.Listaknjiga)
            {
                if (item.Deleted == true)
                {
                    int id = item.Id;
                    sb.Append($"<tr><td>{item.Id}</td><td>{item.Name}</td><td>{item.Price}</td><td>{item.Genre}</td>");

                }

            }

            sb.Append("</table>");
            sb.Append("<a href='/Bookstore/Index'> Link for homepage</a>");
            return Content(sb.ToString());


        }
    }
}