﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara_Domaci.Models
{
    public class Book
    {
        public static int SId = 0;
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public string Genre { get; set; }
        public bool Deleted { get; set; }

        public Book()
        {
        }

        public Book(int id, string name, int price, string genre, bool deleted)
        {
            Id = id;
            Name = name;
            Price = price;
            Genre = genre;
            Deleted = deleted;
        }

        public Book(string name, int price, string genre)
        {
            Id = SId++;
            Name = name;
            Price = price;
            Genre = genre;
            Deleted = false;
        }

        public override string ToString()
        {
            return "Ime: " + Name;
        }

    }
}