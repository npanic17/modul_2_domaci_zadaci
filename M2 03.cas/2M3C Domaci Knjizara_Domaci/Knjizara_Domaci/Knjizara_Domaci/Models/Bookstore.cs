﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara_Domaci.Models
{
    public class Bookstore
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public  static List<Book> Listaknjiga { get; set; }= new List<Book>();
        public static List<Book> ListaObrisanihknjiga { get; set; } = new List<Book>();
    }
}