﻿using M2C5_DomaciKnjizara.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2C5_DomaciKnjizara.ViewModels
{
    public class BookGenreViewModel
    {
        public Book Book { get; set; }
        public Genre Genre { get; set; }
        public List<Genre> GenreList { get; set; }
        public int SelectedGenreId { get; set; }
    }
}