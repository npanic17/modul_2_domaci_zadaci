﻿using M2C5_DomaciKnjizara.Models;
using M2C5_DomaciKnjizara.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace M2C5_DomaciKnjizara.Controllers
{
    public class BookstoreController : Controller
    {
        // GET: Bookstore
        public ActionResult Index()
        {
            BookGenreViewModel BookGenreViewModel = new BookGenreViewModel();
            BookGenreViewModel.GenreList = Project.GenreList;
            return View(BookGenreViewModel);
        }

        public ActionResult List()
        {

            return View(Project.BookList);
        }

        [HttpPost]
        public ActionResult Add(BookGenreViewModel BookGenreViewModel)
        {
            Book newBook = BookGenreViewModel.Book;
            Genre choosedGenre;

            foreach (var item in Project.GenreList)
            {
                if (item.Id == BookGenreViewModel.SelectedGenreId)
                {
                    choosedGenre = item;
                    newBook.Genre = choosedGenre;
                }
            }

            newBook.Id = GetHashCode();
            Project.BookList.Add(newBook);

            return RedirectToAction("List");
        }

        public ActionResult Delete(int id)
        {
            // int index = Int32.Parse(id);
            foreach (var item in Project.BookList)
            {
                if (item.Id == id) item.Deleted = true;
            }
            return RedirectToAction("List");
        }

        public ActionResult Deleted()
        {
            return View(Project.BookList);
        }

        public ActionResult Edit(int id)
        {
            BookGenreViewModel BookGenreViewModel = new BookGenreViewModel();
            BookGenreViewModel.GenreList = Project.GenreList;
            foreach (var item in Project.BookList)
            {
                if (item.Id == id) BookGenreViewModel.Book = item;
            }

            return View(BookGenreViewModel);
        }

        [HttpPost]
        public ActionResult Edit(BookGenreViewModel BookGenreViewModel)
        {
            Book EditedBook = BookGenreViewModel.Book;
            Genre choosedGenre = null;

            foreach (var item in Project.GenreList)
            {
                if (item.Id == BookGenreViewModel.SelectedGenreId)
                {
                    choosedGenre = item;
                }
            }

            foreach (var item in Project.BookList)
            {
                if (item.Id == EditedBook.Id)
                {
                    item.Name = EditedBook.Name;
                    item.Price = EditedBook.Price;
                    item.Genre = choosedGenre;
                }
            }
            return RedirectToAction("List");
        }


        public ActionResult Sort(string BookName, string BookPrice)
        {
            if (BookName == null && BookPrice == null)
            {
                return RedirectToAction("List");
            }
            else if (BookName != null && BookPrice == null)
            {
                List<Book> sortedList = null;
                if (BookName == "asc")
                {
                    sortedList = Project.BookList.OrderBy(x => x.Name).ToList();
                }
                else
                {
                    sortedList = Project.BookList.OrderByDescending(x => x.Name).ToList();
                }

                Project.BookList = sortedList;
                return RedirectToAction("List");
            }
            else if (BookName == null && BookPrice != null)
            {
                List<Book> sortedList = null;
                if (BookPrice == "asc")
                {
                    sortedList = Project.BookList.OrderBy(x => x.Price).ToList();
                }
                else
                {
                    sortedList = Project.BookList.OrderByDescending(x => x.Price).ToList();
                }
                Project.BookList = sortedList;
                return RedirectToAction("List");
            }

            return View();
        }

    }
}