﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using M2C5_DomaciKnjizara.Models;
using M2C5_DomaciKnjizara.ViewModels;

namespace M2C5_DomaciKnjizara.Controllers
{
    public class GenreController : Controller
    {
        // GET: Genre
        public ActionResult Index()
        {
            BookGenreViewModel BookGenreViewModel = new BookGenreViewModel();
            BookGenreViewModel.GenreList = Project.GenreList;
            return View(BookGenreViewModel);
        }

        public ActionResult Delete(int id)
        {
            // int index = Int32.Parse(id);
            foreach (var item in Project.BookList)
            {
                if (item.Genre.Id == id) item.Deleted = true;
            }
            Project.GenreList.RemoveAll(x => x.Id == id);
            return RedirectToAction("Index");
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(BookGenreViewModel BookGenreViewModel)
        {
            Genre newGenre = BookGenreViewModel.Genre;
            newGenre.Id = GetHashCode();
            Project.GenreList.Add(newGenre);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            BookGenreViewModel BookGenreViewModel = new BookGenreViewModel();
            BookGenreViewModel.SelectedGenreId = id;
            foreach (var item in Project.GenreList)
            {
                if (item.Id == id) BookGenreViewModel.Genre = item;
            }

            return View(BookGenreViewModel);
        }

        [HttpPost]
        public ActionResult Edit(BookGenreViewModel BookGenreViewModel)
        {
            foreach (var item in Project.GenreList)
            {
                if (item.Id == BookGenreViewModel.Genre.Id) item.Name = BookGenreViewModel.Genre.Name;
            }

            return RedirectToAction("Index");
        }

    }
}