﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2C5_DomaciKnjizara.Models
{
    public class Project
    {
        public static List<Genre> GenreList = new List<Genre>()
        {
            new Genre() { Id = 0, Name = "Science"},
            new Genre() { Id = 1, Name = "Comedy"},
            new Genre() { Id = 2, Name = "Horror"}
        };

        public static List<Book> BookList = new List<Book>();
    }
}