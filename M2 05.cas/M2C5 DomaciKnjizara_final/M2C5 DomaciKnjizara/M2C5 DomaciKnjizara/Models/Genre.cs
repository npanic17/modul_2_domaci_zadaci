﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2C5_DomaciKnjizara.Models
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Book> Listaknjiga { get; set; }
    }
}