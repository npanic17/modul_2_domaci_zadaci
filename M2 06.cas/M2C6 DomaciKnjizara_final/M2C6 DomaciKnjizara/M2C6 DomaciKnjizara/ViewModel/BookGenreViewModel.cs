﻿using M2C6_DomaciKnjizara.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2C6_DomaciKnjizara.ViewModels
{
    public class BookGenreViewModel
    {
        //public BookGenreViewModel(Book book, Genre genre, List<Genre> genreList, int selectedGenreId)
        //{
        //    Book = book;
        //    Genre = genre;
        //    GenreList = genreList;
        //    SelectedGenreId = selectedGenreId;
        //}

        //public BookGenreViewModel()
        //{
        //}

        public Book Book { get; set; } = new Book();
        public Genre Genre { get; set; } = new Genre();
        public List<Genre> GenreList { get; set; } = new List<Genre>();
        public int SelectedGenreId { get; set; }
    }
}