﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace M2C6_DomaciKnjizara.Models
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Book> Listaknjiga { get; set; } = new List<Book>();

        public Genre() { }
        public Genre(int id, string name)
        {
            Id = id;
            Name = name;
        }

    }
}