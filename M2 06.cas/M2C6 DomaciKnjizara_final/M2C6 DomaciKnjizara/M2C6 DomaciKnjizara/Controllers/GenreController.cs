﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using M2C6_DomaciKnjizara.Models;
using M2C6_DomaciKnjizara.ViewModels;

namespace M2C6_DomaciKnjizara.Controllers
{
    public class GenreController : Controller
    {
        string connectionString = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ConnectionString;

        // GET: Genre
        public ActionResult Index()
        {
            BookGenreViewModel BookGenreViewModel = new BookGenreViewModel();

            string query = "SELECT * FROM Genre";  // upit nad bazom

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Genre"); // 'Genre' je naziv tabele u dataset-u
                    dt = ds.Tables["Genre"];    // formiraj DataTable na osnovu Genre tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }
            }

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int GenreId = int.Parse(dataRow["GenreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string GenreName = dataRow["GenreName"].ToString();
                BookGenreViewModel.GenreList.Add(new Genre(GenreId, GenreName));
            }

            return View(BookGenreViewModel);
        }

        public ActionResult Delete(int id)
        {
            string query = "DELETE FROM Book WHERE GenreId=@GenreId; DELETE FROM Genre WHERE GenreId=@GenreId ";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreId", id);
                    con.Open();
                    cmd.ExecuteScalar();
                    con.Close();
                }
            }
            return RedirectToAction("Index");
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(BookGenreViewModel BookGenreViewModel)
        {

            string query1 = "INSERT INTO Genre (GenreName) VALUES(@GenreName); ";
            query1 += "SELECT SCOPE_IDENTITY()";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {

                    cmd.CommandText = query1;
                    cmd.Parameters.AddWithValue("@GenreName", BookGenreViewModel.Genre.Name);
                    con.Open();
                    cmd.ExecuteScalar();
                    con.Close();
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {

            string query = "SELECT * FROM Genre WHERE GenreId=@GenreId";  // upit nad bazom
            Genre genre = new Genre();
            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreId", id);
                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Genre"); // 'Genre' je naziv tabele u dataset-u
                    dt = ds.Tables["Genre"];    // formiraj DataTable na osnovu Genre tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }
            }

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int GenreId = int.Parse(dataRow["GenreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string GenreName = dataRow["GenreName"].ToString();
                genre = new Genre(GenreId, GenreName);
            }
            return View(genre);
        }

        [HttpPost]
        public ActionResult Edit(Genre Genre)
        {
            string query = "UPDATE Genre SET GenreName=@GenreName WHERE GenreId=@GenreId";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreName", Genre.Name);
                    cmd.Parameters.AddWithValue("@GenreId", Genre.Id);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            return RedirectToAction("Index");
        }

    }
}