﻿using M2C6_DomaciKnjizara.Models;
using M2C6_DomaciKnjizara.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace M2C6_DomaciKnjizara.Controllers
{
    public class BookstoreController : Controller
    {
        // GET: Bookstore
        public ActionResult Index()
        {
            string query = "SELECT * FROM Genre";  // upit nad bazom
            string connectionString = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ConnectionString;

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Genre"); // 'Genre' je naziv tabele u dataset-u
                    dt = ds.Tables["Genre"];    // formiraj DataTable na osnovu Genre tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }
            }

            BookGenreViewModel BookGenreViewModel = new BookGenreViewModel();

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int GenreId = int.Parse(dataRow["GenreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string GenreName = dataRow["GenreName"].ToString();
                BookGenreViewModel.GenreList.Add(new Genre(GenreId, GenreName));
            }
            return View(BookGenreViewModel);
        }

        public ActionResult List()
        {
            string query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId";
            string connectionString = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ConnectionString;

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;
                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }
            }

            List<Book> BookList = new List<Book>();
            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int BookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string BookName = dataRow["BookName"].ToString();
                int BookPrice = int.Parse(dataRow["BookPrice"].ToString());
                bool BookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                Genre g = new Genre
                {
                    Name = dataRow["GenreName"].ToString()
                };


                BookList.Add(new Book(BookId, BookName, BookPrice, g, BookDeleted));
            }

            return View(BookList);
        }

        [HttpPost]
        public ActionResult Add(BookGenreViewModel BookGenreViewModel)
        {
            Book newBook = BookGenreViewModel.Book;
            //Genre choosedGenre;

            #region SELECT * FROM Genre 
            string query = "SELECT * FROM Genre";  // upit nad bazom
            string connectionString = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ConnectionString;

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Genre"); // 'Genre' je naziv tabele u dataset-u
                    dt = ds.Tables["Genre"];    // formiraj DataTable na osnovu Genre tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }
            }

            List<Genre> GenreList = new List<Genre>();

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int GenreId = int.Parse(dataRow["GenreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string GenreName = dataRow["GenreName"].ToString();
                GenreList.Add(new Genre(GenreId, GenreName));
            }
            #endregion

            foreach (var item in GenreList)
            {
                if (item.Id == BookGenreViewModel.SelectedGenreId)
                {
                    newBook.Genre = item;
                }
            }

            #region INSERT new book
            string query1 = "INSERT INTO Book (BookName,BookPrice,GenreId,BookDeleted) VALUES(@BookName, @BookPrice,@GenreId, @BookDeleted); ";
            query1 += "SELECT SCOPE_IDENTITY()";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {

                    cmd.CommandText = query1;
                    //SqlCommand cmd = new SqlCommand(query, con);
                    cmd.Parameters.AddWithValue("@BookName", newBook.Name);
                    cmd.Parameters.AddWithValue("@BookPrice", newBook.Price);
                    cmd.Parameters.AddWithValue("@GenreId", newBook.Genre.Id);
                    cmd.Parameters.AddWithValue("@BookDeleted", newBook.Deleted);

                    con.Open();
                    cmd.ExecuteScalar();
                    con.Close();
                }
            }
            #endregion

            return RedirectToAction("List");
        }

        public ActionResult Delete(int id)
        {
            string query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId";
            string connectionString = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ConnectionString;

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;
                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }
            }

            List<Book> BookList = new List<Book>();
            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int BookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string BookName = dataRow["BookName"].ToString();
                int BookPrice = int.Parse(dataRow["BookPrice"].ToString());
                bool BookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                Genre g = new Genre
                {
                    Name = dataRow["GenreName"].ToString()
                };


                BookList.Add(new Book(BookId, BookName, BookPrice, g, BookDeleted));
            }

            // int index = Int32.Parse(id);
            foreach (var item in BookList)
            {
                if (item.Id == id)
                {
                    string query1 = $"UPDATE Book SET BookDeleted=1 WHERE BookId={id}";
                    using (SqlConnection con = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = con.CreateCommand())
                        {

                            cmd.CommandText = query1;
                            //SqlCommand cmd = new SqlCommand(query, con);

                            con.Open();
                            cmd.ExecuteNonQuery();
                            con.Close();
                        }
                    }

                }
            }
            return RedirectToAction("List");
        }

        public ActionResult Deleted()
        {
            string query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId";
            string connectionString = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ConnectionString;

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;
                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }
            }

            List<Book> BookList = new List<Book>();
            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int BookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string BookName = dataRow["BookName"].ToString();
                int BookPrice = int.Parse(dataRow["BookPrice"].ToString());
                bool BookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                Genre g = new Genre
                {
                    Name = dataRow["GenreName"].ToString()
                };


                BookList.Add(new Book(BookId, BookName, BookPrice, g, BookDeleted));
            }
            return View(BookList);
        }

        public ActionResult Edit(int id)
        {
            #region get genre
            string query = "SELECT * FROM Genre";  // upit nad bazom
            string connectionString = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ConnectionString;

            DataTable dt = new DataTable(); // objekti u 
            DataSet ds = new DataSet();     // koje smestam podatke

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "Genre"); // 'Genre' je naziv tabele u dataset-u
                    dt = ds.Tables["Genre"];    // formiraj DataTable na osnovu Genre tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }
            }

            BookGenreViewModel BookGenreViewModel = new BookGenreViewModel();

            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int GenreId = int.Parse(dataRow["GenreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string GenreName = dataRow["GenreName"].ToString();
                BookGenreViewModel.GenreList.Add(new Genre(GenreId, GenreName));
            }
            #endregion
                        
            #region get book
            string query1 = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId WHERE BookId=@BookId";

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query1;
                    cmd.Parameters.AddWithValue("@BookId",id);
                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;
                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }
            }

            
            foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
            {
                int BookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                string BookName = dataRow["BookName"].ToString();
                int BookPrice = int.Parse(dataRow["BookPrice"].ToString());
                bool BookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                int GenreId = int.Parse(dataRow["GenreId"].ToString());

                BookGenreViewModel.Book.Id = BookId;
                BookGenreViewModel.Book.Name = BookName;
                BookGenreViewModel.Book.Price = BookPrice;
                BookGenreViewModel.SelectedGenreId = GenreId;
            }
            #endregion

            return View(BookGenreViewModel);
        }

        [HttpPost]
        public ActionResult Edit(BookGenreViewModel BookGenreViewModel)
        {
            Book EditedBook = BookGenreViewModel.Book;

            string connectionString = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ConnectionString;
            string query = $"UPDATE Book SET BookName='{BookGenreViewModel.Book.Name}',BookPrice={BookGenreViewModel.Book.Price},GenreId={BookGenreViewModel.SelectedGenreId} WHERE BookId={BookGenreViewModel.Book.Id}";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }


            return RedirectToAction("List");
        }
        
        public ActionResult Sort(string BookName, string BookPrice)
        {

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            SqlDataAdapter dadapter = new SqlDataAdapter();
            string query = null;
            string connectionString = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ConnectionString;
            if (BookName == null && BookPrice == null)
            {
                return RedirectToAction("List");
            }
            else if (BookName != null && BookPrice == null)
            {
                if (BookName == "asc")
                {
                    query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId ORDER BY BookName";
                }
                else
                {
                    query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId ORDER BY BookName DESC";
                }
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;
                        dadapter.SelectCommand = cmd;
                        dadapter.Fill(ds, "Book");
                        dt = ds.Tables["Book"];
                        con.Close();
                    }
                }
                List<Book> BookList = new List<Book>();
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int BookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string BookN = dataRow["BookName"].ToString();
                    int BookP = int.Parse(dataRow["BookPrice"].ToString());
                    bool BookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                    Genre g = new Genre
                    {
                        Name = dataRow["GenreName"].ToString()
                    };

                    BookList.Add(new Book(BookId, BookN, BookP, g, BookDeleted));
                }
                return View("List", BookList);
            }
            else if (BookName == null && BookPrice != null)
            {
                if (BookPrice == "asc")
                {
                    query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId ORDER BY BookPrice";
                }
                else
                {
                    query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId ORDER BY BookPrice DESC";
                }

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;
                        dadapter.SelectCommand = cmd;
                        dadapter.Fill(ds, "Book");
                        dt = ds.Tables["Book"];
                        con.Close();
                    }
                }
                List<Book> BookList = new List<Book>();
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int BookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string BookN = dataRow["BookName"].ToString();
                    int BookP = int.Parse(dataRow["BookPrice"].ToString());
                    bool BookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                    Genre g = new Genre
                    {
                        Name = dataRow["GenreName"].ToString()
                    };
                    BookList.Add(new Book(BookId, BookN, BookP, g, BookDeleted));
                }
                return View("List", BookList);
            }
            return RedirectToAction("List");
        }
    }
}