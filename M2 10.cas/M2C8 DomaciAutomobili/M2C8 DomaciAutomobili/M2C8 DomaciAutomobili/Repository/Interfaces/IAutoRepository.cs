﻿using M2C8_DomaciAutomobili.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2C8_DomaciAutomobili.Repository.Interfaces
{
    interface IAutoRepository
    {
        IEnumerable<Auto> GetAll();
        Auto GetById(int id);
        bool Create(Auto Auto);
        void Update(Auto Auto);
        void Delete(int id);
    }
}
