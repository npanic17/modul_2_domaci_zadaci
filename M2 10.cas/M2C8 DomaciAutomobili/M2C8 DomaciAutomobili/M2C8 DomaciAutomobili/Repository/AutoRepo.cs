﻿using M2C8_DomaciAutomobili.Models;
using M2C8_DomaciAutomobili.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace M2C8_DomaciAutomobili.Repository
{
    public class AutoRepo : IAutoRepository
    {
        TipMotoraRepo TipMotoraRepo = new TipMotoraRepo();
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AutomobiliDBContext"].ToString();
            con = new SqlConnection(constr);
        }

        public IEnumerable<Auto> GetAll()
        {
            List<Auto> ListAutomobila = new List<Auto>();

            try
            {
                Connection();
                string query = "SELECT * FROM Automobili;";

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds, "Auto");
                    dt = ds.Tables["Auto"];

                    foreach (DataRow datarow in dt.Rows)
                    {
                        int id = Int32.Parse(datarow["AutoId"].ToString());
                        string proizvodjac = datarow["AutoProizvodjac"].ToString();
                        string model = datarow["AutoModel"].ToString();
                        int god = Int32.Parse(datarow["AutoGod"].ToString());
                        int kub = Int32.Parse(datarow["AutoKub"].ToString());
                        string boja = datarow["AutoBoja"].ToString();
                        int autotipid = Int32.Parse(datarow["AutoTipId"].ToString());
                        Auto Auto = new Auto() { AutoId = id, AutoProizvodjac = proizvodjac, AutoModel = model, AutoGod = god, AutoKub = kub, AutoBoja = boja, TipMotora = TipMotoraRepo.GetById(autotipid) };
                        ListAutomobila.Add(Auto);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return ListAutomobila;
        }

        public Auto GetById(int id)
        {
            Auto RetVal = new Auto();

            try
            {
                Connection();
                string query = "SELECT * FROM Automobili WHERE AutoId=@Id;";

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Id", id);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds, "Auto");
                    dt = ds.Tables["Auto"];

                    foreach (DataRow datarow in dt.Rows)
                    {
                        int autoid = Int32.Parse(datarow["AutoId"].ToString());
                        string proizvodjac = datarow["AutoProizvodjac"].ToString();
                        string model = datarow["AutoModel"].ToString();
                        int god = Int32.Parse(datarow["AutoGod"].ToString());
                        int kub = Int32.Parse(datarow["AutoKub"].ToString());
                        string boja = datarow["AutoBoja"].ToString();
                        int autotipid = Int32.Parse(datarow["AutoTipId"].ToString());
                        Auto Auto = new Auto() { AutoId = autoid, AutoProizvodjac = proizvodjac, AutoModel = model, AutoGod = god, AutoKub = kub, AutoBoja = boja, TipMotora = TipMotoraRepo.GetById(autotipid) };
                        RetVal = Auto;
                    }
                }
                return RetVal;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }

        }

        public bool Create(Auto Auto)
        {
            try
            {
                Connection();
                string query = "INSERT INTO Automobili (AutoProizvodjac,AutoModel,AutoGod,AutoKub,AutoBoja,AutoTipId) VALUES (@AutoProizvodjac,@AutoModel,@AutoGod,@AutoKub,@AutoBoja,@TipMotora);";
                query += "SELECT SCOPE_IDENTITY()";

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@AutoProizvodjac", Auto.AutoProizvodjac);
                    cmd.Parameters.AddWithValue("@AutoModel", Auto.AutoModel);
                    cmd.Parameters.AddWithValue("@AutoGod", Auto.AutoGod);
                    cmd.Parameters.AddWithValue("@AutoKub", Auto.AutoKub);
                    cmd.Parameters.AddWithValue("@AutoBoja", Auto.AutoBoja);
                    cmd.Parameters.AddWithValue("@TipMotora", Auto.TipMotora.TipId);
                    con.Open();
                    var newCreatedId = cmd.ExecuteScalar();
                    con.Close();
                    if (newCreatedId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        public void Update(Auto Auto)
        {
            try
            {
                Connection();
                string query = "UPDATE Automobili SET AutoProizvodjac = @AutoProizvodjac,AutoModel = @AutoModel,AutoGod = @AutoGod,AutoKub = @AutoKub,AutoBoja = @AutoBoja, AutoTipId = @TipMotora WHERE AutoId = @Id;";

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@AutoProizvodjac", Auto.AutoProizvodjac);
                    cmd.Parameters.AddWithValue("@AutoModel", Auto.AutoModel);
                    cmd.Parameters.AddWithValue("@AutoGod", Auto.AutoGod);
                    cmd.Parameters.AddWithValue("@AutoKub", Auto.AutoKub);
                    cmd.Parameters.AddWithValue("@AutoBoja", Auto.AutoBoja);
                    cmd.Parameters.AddWithValue("@TipMotora", Auto.TipMotora.TipId);
                    cmd.Parameters.AddWithValue("@Id", Auto.AutoId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }


        public void Delete(int id)
        {
            try
            {
                Connection();
                string query = "DELETE Automobili WHERE AutoId=@Id;";

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Id", id);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

    }
}