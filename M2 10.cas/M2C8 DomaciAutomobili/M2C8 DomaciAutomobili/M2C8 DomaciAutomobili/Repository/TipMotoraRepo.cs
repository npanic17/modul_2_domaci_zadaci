﻿using M2C8_DomaciAutomobili.Models;
using M2C8_DomaciAutomobili.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace M2C8_DomaciAutomobili.Repository
{
    public class TipMotoraRepo : ITipMotoraRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AutomobiliDBContext"].ToString();
            con = new SqlConnection(constr);
        }

        public IEnumerable<TipMotora> GetAll()
        {
            List<TipMotora> ListTipMotora = new List<TipMotora>();

            try
            {
                Connection();
                string query = "SELECT * FROM TipMotora;";

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds, "TipMotora");
                    dt = ds.Tables["TipMotora"];

                    foreach (DataRow datarow in dt.Rows)
                    {
                        int id = Int32.Parse(datarow["TipId"].ToString());
                        string naziv = datarow["TipNaziv"].ToString();
                        TipMotora tp = new TipMotora() { TipId = id, TipNaziv = naziv };
                        ListTipMotora.Add(tp);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return ListTipMotora;
        }

        public TipMotora GetById(int id)
        {
            TipMotora RetVal = new TipMotora();

            try
            {
                Connection();
                string query = "SELECT * FROM TipMotora WHERE TipId=@TipId;";

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@TipId", id);
                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = cmd;
                    da.Fill(ds, "TipMotora");
                    dt = ds.Tables["TipMotora"];

                    foreach (DataRow datarow in dt.Rows)
                    {
                        int Id = Int32.Parse(datarow["TipId"].ToString());
                        string naziv = datarow["TipNaziv"].ToString();
                        TipMotora tp = new TipMotora() { TipId = Id, TipNaziv = naziv };
                        RetVal = tp;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return RetVal;
        }

        public bool Create(TipMotora TipMotora)
        {
            try
            {
                Connection();
                string query = "INSERT INTO TipMotora (TipNaziv) VALUES (@Naziv);";
                query += "SELECT SCOPE_IDENTITY()";

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Naziv", TipMotora.TipNaziv);
                    con.Open();
                    var newCreatedId = cmd.ExecuteScalar();
                    con.Close();
                    if (newCreatedId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }

        }

        public void Update(TipMotora TipMotora)
        {
            try
            {
                Connection();
                string query = "UPDATE TipMotora SET TipNaziv = @Naziv WHERE TipId = @Id;";

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Naziv", TipMotora.TipNaziv);
                    cmd.Parameters.AddWithValue("@Id", TipMotora.TipId);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }

        public void Delete(int id)
        {
            try
            {
                Connection();
                string query = "DELETE Automobili WHERE AutoTipId=@Id; DELETE TipMotora WHERE TipId=@Id;";

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Id", id);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw e;
            }
        }
    }
}