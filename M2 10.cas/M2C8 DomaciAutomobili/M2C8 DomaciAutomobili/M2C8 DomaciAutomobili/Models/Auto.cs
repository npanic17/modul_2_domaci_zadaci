﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2C8_DomaciAutomobili.Models
{
    public class Auto
    {
        public int AutoId { get; set; }

        [Required]
        [Display(Name = "Naziv proizvodjaca")]
        [StringLength(20, MinimumLength = 3)]
        [RegularExpression("[A-Z][A-Za-z]*", ErrorMessage = "Prvo slovo mora biti veliko")]
        public string AutoProizvodjac { get; set; }

        [Required]
        [Display(Name = "Naziv modela")]
        [StringLength(20, MinimumLength = 3)]
        [RegularExpression("[A-Z][A-Za-z]*", ErrorMessage = "Prvo slovo mora biti veliko")]
        public string AutoModel { get; set; }

        [Required]
        [Display(Name = "Godiste automobila")]
        public int AutoGod { get; set; }

        [Required]
        [Display(Name = "kubikaza automobila")]
        public int AutoKub { get; set; }

        [Required]
        [Display(Name = "Boja automobila")]
        [StringLength(20, MinimumLength = 3)]
        [RegularExpression("[A-Z][A-Za-z]*", ErrorMessage = "Prvo slovo mora biti veliko")]
        public string AutoBoja { get; set; }

        public TipMotora TipMotora { get; set; }
    }
}