$(document).ready(function () {
    $("form").submit(function (event) {

        //Validacija za Automobil
        if ($("#Auto_AutoProizvodjac").val().length < 3) {
            event.preventDefault();
            $("span[data-valmsg-for='Auto.AutoProizvodjac']").text("Morate biti vise od 3 slova").css("color", "red")
        }
        if (!$("#Auto_AutoProizvodjac").val()) {
            event.preventDefault();
            $("span[data-valmsg-for='Auto.AutoProizvodjac']").text("Morate popuniti polje").css("color", "red")
        }
        if ($("#Auto_AutoModel").val().length < 3) {
            event.preventDefault();
            $("span[data-valmsg-for='Auto.AutoModel']").text("Morate biti vise od 3 slova").css("color", "red")
        }
        if (!$("#Auto_AutoModel").val()) {
            event.preventDefault();
            $("span[data-valmsg-for='Auto.AutoModel']").text("Morate popuniti polje").css("color", "red")
        }
        if (!$("#Auto_AutoGod").val()) {
            event.preventDefault();
            $("span[data-valmsg-for='Auto.AutoGod']").text("Morate popuniti polje").css("color", "red")
        }
        if (!$("#Auto_AutoKub").val()) {
            event.preventDefault();
            $("span[data-valmsg-for='Auto.AutoKub']").text("Morate popuniti polje").css("color", "red")
        }
        if ($.isNumeric($("#Auto_AutoBoja").val())) {
            event.preventDefault();
            $("span[data-valmsg-for='Auto.AutoBoja']").text("Morate uneti boju a ne broj!").css("color", "red")
        }
        

        //Validacija za Tip Motora
        if (!$("#TipNaziv").val()) {
            event.preventDefault();
            $("span[data-valmsg-for='TipNaziv']").text("MORATE popuniti polje").css("color", "red")
        }
        if ($("#TipNaziv").val().length  < 3) {
           event.preventDefault();
            $("span[data-valmsg-for='TipNaziv']").text("MORATE biti vise od 3 slova").css("color", "red")
        }

    })
})
