﻿using M2C8_DomaciAutomobili.Models;
using M2C8_DomaciAutomobili.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace M2C8_DomaciAutomobili.Controllers
{
    public class TipMotoraController : Controller
    {
        TipMotoraRepo TipMotoraRepo = new TipMotoraRepo();
        // GET: TipMotora
        public ActionResult Index()
        {
            List<TipMotora> ListTipMotora = new List<TipMotora>();
            ListTipMotora = TipMotoraRepo.GetAll().ToList();
            return View(ListTipMotora);
        }

        public ActionResult Add()
        {

            return View(new TipMotora());
        }

        [HttpPost]
        public ActionResult Add(TipMotora TipMotora)
        {
            if (!ModelState.IsValid)
            {
                return View(TipMotora);
            }
            if (TipMotoraRepo.Create(TipMotora))
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Edit(int id)
        {
            TipMotora TipMotora = TipMotoraRepo.GetById(id);
            return View(TipMotora);
        }

        [HttpPost]
        public ActionResult Edit(TipMotora TipMotora)
        {
            if (!ModelState.IsValid)
            {
                return View(TipMotora);
            }
            TipMotoraRepo.Update(TipMotora);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            TipMotoraRepo.Delete(id);
            return RedirectToAction("Index");
        }

    }
}