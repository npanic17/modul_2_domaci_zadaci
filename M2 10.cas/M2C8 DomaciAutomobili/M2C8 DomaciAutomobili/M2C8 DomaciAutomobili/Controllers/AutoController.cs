﻿using M2C8_DomaciAutomobili.Models;
using M2C8_DomaciAutomobili.Repository;
using M2C8_DomaciAutomobili.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace M2C8_DomaciAutomobili.Controllers
{
    public class AutoController : Controller
    {
        TipMotoraRepo tmrepo = new TipMotoraRepo();
        AutoRepo autoRepo = new AutoRepo();
        // GET: Auto
        public ActionResult Index()
        {
            List<Auto> ListAutomobila = new List<Auto>();
            ListAutomobila = autoRepo.GetAll().ToList();

            return View(ListAutomobila);
        }

        public ActionResult Add()
        {
            AutoTipMotoraViewModel vm = new AutoTipMotoraViewModel() { ListTipMotora=new List<TipMotora>()};
            vm.ListTipMotora = tmrepo.GetAll().ToList();
            return View(vm);
        }

        [HttpPost]
        public ActionResult Add(AutoTipMotoraViewModel AutoTipMotoraViewModel)
        {

            if (!ModelState.IsValid)
            {
                AutoTipMotoraViewModel.ListTipMotora = tmrepo.GetAll().ToList();
                return View(AutoTipMotoraViewModel);
            }
            AutoTipMotoraViewModel.Auto.TipMotora = tmrepo.GetById(AutoTipMotoraViewModel.SelectedTipMotorById);
            if (autoRepo.Create(AutoTipMotoraViewModel.Auto))
            {
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Edit(int id)
        {
            Auto AutoById = autoRepo.GetById(id);
            AutoTipMotoraViewModel AutoTipMotoraViewModel = new AutoTipMotoraViewModel() { Auto = AutoById, ListTipMotora = tmrepo.GetAll().ToList() };

            return View(AutoTipMotoraViewModel);
        }

        [HttpPost]
        public ActionResult Edit(AutoTipMotoraViewModel AutoTipMotoraViewModel)
        {
            AutoTipMotoraViewModel.Auto.TipMotora = new TipMotora() { TipId = AutoTipMotoraViewModel.SelectedTipMotorById };
            autoRepo.Update(AutoTipMotoraViewModel.Auto);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            autoRepo.Delete(id);
            return RedirectToAction("Index");
        }

    }
}