﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using M2C8_DomaciAutomobili.Models;

namespace M2C8_DomaciAutomobili.ViewModel
{
    public class AutoTipMotoraViewModel
    {
        public Auto Auto { get; set; }
        public List<TipMotora> ListTipMotora { get; set; }
        public int SelectedTipMotorById { get; set; }
    }
}