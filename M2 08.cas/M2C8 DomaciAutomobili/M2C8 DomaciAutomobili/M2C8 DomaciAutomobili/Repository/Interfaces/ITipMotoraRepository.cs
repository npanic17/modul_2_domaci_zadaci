﻿using M2C8_DomaciAutomobili.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2C8_DomaciAutomobili.Repository.Interfaces
{
    interface ITipMotoraRepository
    {
        IEnumerable<TipMotora> GetAll();
        TipMotora GetById(int id);
        bool Create(TipMotora TipMotora);
        void Update(TipMotora TipMotora);
        void Delete(int id);
    }
}
