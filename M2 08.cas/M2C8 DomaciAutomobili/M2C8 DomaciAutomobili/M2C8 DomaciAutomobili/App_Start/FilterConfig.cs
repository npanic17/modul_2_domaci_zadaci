﻿using System.Web;
using System.Web.Mvc;

namespace M2C8_DomaciAutomobili
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
