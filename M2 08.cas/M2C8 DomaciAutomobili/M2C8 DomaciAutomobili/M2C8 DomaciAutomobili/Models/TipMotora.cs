﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2C8_DomaciAutomobili.Models
{
    public class TipMotora
    {
        public int TipId { get; set; }

        [Required]
        [Display(Name = "Naziv tipa motora")]
        [StringLength(20, MinimumLength =3)]
        [RegularExpression("[A-Z][A-Za-z]*",ErrorMessage ="Prvo slovo mora biti veliko")]
        public string TipNaziv { get; set; }
    }
}