﻿using M2C8_DomaciKnjizara.Models;
using M2C8_DomaciKnjizara.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace M2C8_DomaciKnjizara.Repository
{
    public class GenreRepo : IGenreRepository
    {
        private SqlConnection con;
        private void connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ToString();
            con = new SqlConnection(constr);
        }

        public IEnumerable<Genre> GetAll()
        {
            List<Genre> GenreList = new List<Genre>();

            try
            {
                string query = "SELECT * FROM Genre;";
                connection();
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;
                    dadapter.Fill(ds, "Genre");
                    dt = ds.Tables["Genre"];
                    con.Close();
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int GenreId = int.Parse(dataRow["GenreId"].ToString());
                    string GenreName = dataRow["GenreName"].ToString();
                    GenreList.Add(new Genre() { Id = GenreId, Name = GenreName });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja zanra. {ex.StackTrace}");
                throw ex;
            }
            return GenreList;
        }

        public Genre GetById(int id)
        {
            Genre Genre = null;

            try
            {
                string query = "SELECT * FROM Genre WHERE GenreId=@GenreId ;";
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreId", id);
                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;
                    dadapter.Fill(ds, "Genre");
                    dt = ds.Tables["Genre"];
                    con.Close();
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int GenreId = int.Parse(dataRow["GenreId"].ToString());
                    string GenreName = dataRow["GenreName"].ToString();
                    Genre = new Genre() { Id = GenreId, Name = GenreName };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja zanra. {ex.StackTrace}");
                throw ex;
            }
            return Genre;

        }

        public bool Create(Genre Genre)
        {
            try
            {
                string query = "INSERT INTO Genre (GenreName) VALUES(@GenreName); ";
                query += "SELECT SCOPE_IDENTITY()";
                connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreName", Genre.Name);
                    con.Open();
                    var newCreatedId = cmd.ExecuteScalar();
                    con.Close();

                    if (newCreatedId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom upisa novog zanra. {ex.StackTrace}");
                throw ex;
            }

        }

        public void Update(Genre Genre)
        {
            try
            {
                string query = "UPDATE Genre SET GenreName=@GenreName WHERE GenreId=@GenreId";
                connection();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreName", Genre.Name);
                    cmd.Parameters.AddWithValue("@GenreId", Genre.Id);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom update-a zanra. {ex.StackTrace}");
                throw ex;
            }
        }

        public void Delete(int id)
        {
            try
            {
                string query = "DELETE FROM Book WHERE GenreId=@GenreId; DELETE FROM Genre WHERE GenreId=@GenreId ";
                connection();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreId", id);
                    con.Open();
                    cmd.ExecuteScalar();
                    con.Close();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja zanra. {ex.StackTrace}");
                throw ex;
            }
        }
    }
}