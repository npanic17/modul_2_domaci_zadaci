﻿using M2C8_DomaciKnjizara.Models;
using M2C8_DomaciKnjizara.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace M2C8_DomaciKnjizara.Repository
{
    public class BookstoreRepo //: IBookstoreRepository
    {
        private GenreRepo genrepo = new GenreRepo();
        private SqlConnection con;
        private void connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ToString();
            con = new SqlConnection(constr);
        }

        public IEnumerable<Book> GetAll()
        {
            List<Book> BookList = new List<Book>();

            try
            {
                string query = "SELECT * FROM Book;";
                connection();
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();


                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;
                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int BookId = int.Parse(dataRow["BookId"].ToString());
                    string BookName = dataRow["BookName"].ToString();
                    int BookPrice = int.Parse(dataRow["BookPrice"].ToString());
                    bool BookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                    int GenreId = int.Parse(dataRow["GenreId"].ToString());
                    BookList.Add(new Book() { Id = BookId, Name = BookName, Price = BookPrice, Deleted = BookDeleted,Genre=genrepo.GetById(GenreId)});
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja knjiga. {ex.StackTrace}");
                throw ex;
            }
            return BookList;
        }

        public Book GetById(int id)
        {
            Book Book = null;

            try
            {
                string query = "SELECT * FROM Book WHERE BookId=@BookId ;";
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookId", id);
                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;
                    dadapter.Fill(ds, "Book");
                    dt = ds.Tables["Book"];
                    con.Close();
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int BookId = int.Parse(dataRow["BookId"].ToString());
                    string BookName = dataRow["BookName"].ToString();
                    int BookPrice = int.Parse(dataRow["BookPrice"].ToString());
                    bool BookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                    int GenreId = int.Parse(dataRow["GenreId"].ToString());

                    Book = new Book() { Id = BookId, Name = BookName, Price = BookPrice, Genre=new Genre() { Id=GenreId}, Deleted = BookDeleted };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja 1 knjige. {ex.StackTrace}");
                throw ex;
            }
            return Book;

        }

        public bool Create(Book newBook)
        {
            try
            {
                string query = "INSERT INTO Book (BookName,BookPrice,GenreId,BookDeleted) VALUES(@BookName, @BookPrice,@GenreId, @BookDeleted); ";
                query += "SELECT SCOPE_IDENTITY()";
                connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookName", newBook.Name);
                    cmd.Parameters.AddWithValue("@BookPrice", newBook.Price);
                    cmd.Parameters.AddWithValue("@GenreId", newBook.Genre.Id);
                    cmd.Parameters.AddWithValue("@BookDeleted", newBook.Deleted);
                    con.Open();
                    var newCreatedId = cmd.ExecuteScalar();
                    con.Close();

                    if (newCreatedId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom upisa nove knjige. {ex.StackTrace}");
                throw ex;
            }

        }

        public void Update(Book Book)
        {
            try
            {
                string query = "UPDATE Book SET BookName=@BookName,BookPrice=@BookPrice,GenreId=@GenreId WHERE BookId=@BookId;";
                connection();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookName", Book.Name);
                    cmd.Parameters.AddWithValue("@BookPrice", Book.Price);
                    cmd.Parameters.AddWithValue("@GenreId", Book.Genre.Id);
                    cmd.Parameters.AddWithValue("@BookId", Book.Id);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom update-a knjige. {ex.StackTrace}");
                throw ex;
            }
        }

        public void Delete(int id)
        {
            try
            {
                string query = "DELETE FROM Book WHERE BookId=@BookId; ";
                connection();
                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookId", id);
                    con.Open();
                    cmd.ExecuteScalar();
                    con.Close();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja knjige. {ex.StackTrace}");
                throw ex;
            }
        }
    }
}