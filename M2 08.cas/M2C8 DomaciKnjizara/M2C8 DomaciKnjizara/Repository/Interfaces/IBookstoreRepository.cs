﻿using M2C8_DomaciKnjizara.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace M2C8_DomaciKnjizara.Repository.Interfaces
{
    interface IBookstoreRepository
    {
        IEnumerable<Book> GetAll();
        Book GetById(int id);
        bool Create(Book Book);
        void Update(Book Book);
        void Delete(int id);
    }
}
