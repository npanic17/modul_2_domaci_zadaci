﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using M2C8_DomaciKnjizara.Models;
using M2C8_DomaciKnjizara.Repository;
using M2C8_DomaciKnjizara.ViewModels;

namespace M2C8_DomaciKnjizara.Controllers
{
    public class GenreController : Controller
    {
        private GenreRepo genrepo = new GenreRepo();
        // GET: Genre
        public ActionResult Index()
        {
            BookGenreViewModel BookGenreViewModel = new BookGenreViewModel();
            BookGenreViewModel.GenreList = genrepo.GetAll().ToList();

            return View(BookGenreViewModel);
        }

        public ActionResult Delete(int id)
        {
            genrepo.Delete(id);

            return RedirectToAction("Index");
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Add(BookGenreViewModel BookGenreViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(BookGenreViewModel);
            }

            if (genrepo.Create(BookGenreViewModel.Genre))
            {
                return RedirectToAction("Index");
            }
            return View();

        }

        public ActionResult Edit(int id)
        {
            Genre genre = genrepo.GetById(id);

            return View(genre);
        }

        [HttpPost]
        public ActionResult Edit(Genre Genre)
        {
            if (ModelState.IsValid)
            {
                genrepo.Update(Genre);

                return RedirectToAction("Index");
                
            }
            return View(Genre);

        }

    }
}