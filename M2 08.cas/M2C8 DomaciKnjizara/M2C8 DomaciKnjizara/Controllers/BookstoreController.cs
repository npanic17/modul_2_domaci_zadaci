﻿using M2C8_DomaciKnjizara.Models;
using M2C8_DomaciKnjizara.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using M2C8_DomaciKnjizara.Repository;

namespace M2C8_DomaciKnjizara.Controllers
{
    public class BookstoreController : Controller
    {
        private BookstoreRepo bookrepo = new BookstoreRepo();
        private GenreRepo genrepo = new GenreRepo();
        // GET: Bookstore
        public ActionResult Index()
        {
            BookGenreViewModel BookGenreViewModel = new BookGenreViewModel();
            BookGenreViewModel.GenreList = genrepo.GetAll().ToList();

            return View(BookGenreViewModel);
        }

        public ActionResult List()
        {
            List<Book> BookList = bookrepo.GetAll().ToList();

            return View(BookList);
        }

        [HttpPost]
        public ActionResult Add(BookGenreViewModel BookGenreViewModel)
        {
            if (bookrepo.Create(BookGenreViewModel.Book))
            {
                return RedirectToAction("List");
            }
            return View();
        }

        public ActionResult Delete(int id)
        {
            bookrepo.Delete(id);
            return RedirectToAction("List");
        }

        public ActionResult Deleted()
        {
            List<Book> BookList = new List<Book>();
            BookList = bookrepo.GetAll().ToList();

            return View(BookList);
        }

        public ActionResult Edit(int id)
        {
            BookGenreViewModel BookGenreViewModel = new BookGenreViewModel();
            BookGenreViewModel.GenreList = genrepo.GetAll().ToList();
            BookGenreViewModel.Book = bookrepo.GetById(id);
            BookGenreViewModel.SelectedGenreId = BookGenreViewModel.Book.Genre.Id;

            return View(BookGenreViewModel);
        }

        [HttpPost]
        public ActionResult Edit(BookGenreViewModel BookGenreViewModel)
        {
            BookGenreViewModel.Book.Genre = new Genre() { Id = BookGenreViewModel.SelectedGenreId };
            bookrepo.Update(BookGenreViewModel.Book);

            return RedirectToAction("List");
        }

        public ActionResult Sort(string BookName, string BookPrice)
        {

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            SqlDataAdapter dadapter = new SqlDataAdapter();
            string query = null;
            string connectionString = ConfigurationManager.ConnectionStrings["KnjizaraDbContext"].ConnectionString;
            if (BookName == null && BookPrice == null)
            {
                return RedirectToAction("List");
            }
            else if (BookName != null && BookPrice == null)
            {
                if (BookName == "asc")
                {
                    query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId ORDER BY BookName";
                }
                else
                {
                    query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId ORDER BY BookName DESC";
                }
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;
                        dadapter.SelectCommand = cmd;
                        dadapter.Fill(ds, "Book");
                        dt = ds.Tables["Book"];
                        con.Close();
                    }
                }
                List<Book> BookList = new List<Book>();
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int BookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string BookN = dataRow["BookName"].ToString();
                    int BookP = int.Parse(dataRow["BookPrice"].ToString());
                    bool BookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                    Genre g = new Genre
                    {
                        Name = dataRow["GenreName"].ToString()
                    };

                    BookList.Add(new Book(BookId, BookN, BookP, g, BookDeleted));
                }
                return View("List", BookList);
            }
            else if (BookName == null && BookPrice != null)
            {
                if (BookPrice == "asc")
                {
                    query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId ORDER BY BookPrice";
                }
                else
                {
                    query = "SELECT * FROM Book JOIN Genre ON Book.GenreId=Genre.GenreId ORDER BY BookPrice DESC";
                }

                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = con.CreateCommand())
                    {
                        cmd.CommandText = query;
                        dadapter.SelectCommand = cmd;
                        dadapter.Fill(ds, "Book");
                        dt = ds.Tables["Book"];
                        con.Close();
                    }
                }
                List<Book> BookList = new List<Book>();
                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int BookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string BookN = dataRow["BookName"].ToString();
                    int BookP = int.Parse(dataRow["BookPrice"].ToString());
                    bool BookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                    Genre g = new Genre
                    {
                        Name = dataRow["GenreName"].ToString()
                    };
                    BookList.Add(new Book(BookId, BookN, BookP, g, BookDeleted));
                }
                return View("List", BookList);
            }
            return RedirectToAction("List");
        }
    }
}