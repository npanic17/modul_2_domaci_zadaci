﻿CREATE TABLE [dbo].[Book]
(
	[BookId] INT IDENTITY(1,1) PRIMARY KEY,
	[BookName] NVARCHAR(80) NOT NULL, 
	[BookPrice] NUMERIC NOT NULL, 
	[GenreId] INT NOT NULL, 
	[BookDeleted] BIT NOT NULL, 
	FOREIGN KEY(GenreId) REFERENCES dbo.Genre(GenreId)
)
