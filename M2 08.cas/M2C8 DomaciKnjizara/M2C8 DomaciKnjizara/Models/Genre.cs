﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2C8_DomaciKnjizara.Models
{
    public class Genre
    {
        public int Id { get; set; }

        [Required] //Ime je obavezan parametar
        [StringLength(20, MinimumLength = 3)] //Minimum 3 karaktera, a Maksimum 20
        [RegularExpression("[A-Z][A-Za-z]*")]  //Ime mora da počne velikim slovom
        public string Name { get; set; }
    }
}