﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace M2C8_DomaciKnjizara.Models
{
    public class Book
    {
        //public static int SId = 0;
        public int Id { get; set; }

        [Required] //Ime je obavezan parametar
        [StringLength(20, MinimumLength = 3)] //Minimum 3 karaktera, a Maksimum 20
        [RegularExpression("[A-Z][A-Za-z]*")] //Ime mora da počne velikim slovom
        public string Name { get; set; }

        [Required] //Ime je obavezan parametar
        public int Price { get; set; }
        public Genre Genre { get; set; }
        public bool Deleted { get; set; }

        public Book()
        {
        }

        public Book(int id, string name, int price, Genre genre, bool deleted)
        {
            Id = id;
            Name = name;
            Price = price;
            Genre = genre;
            Deleted = deleted;
        }

        public Book(string name, int price, Genre genre)
        {
            //Id = SId++;
            Name = name;
            Price = price;
            Genre = genre;
            Deleted = false;
        }



    }
}